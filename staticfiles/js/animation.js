$(document).ready(function(){
    $("#sos").click(function(){
        $("#sosExplain").slideToggle({
            duration : 200,
        });
    });
    $("#exp").click(function(){
        $("#expExplain").slideToggle({
            duration: 500,
        });
    });
    $("#ach").click(function(){
        $("#achExplain").slideToggle({
            duration: 200,
        });
    });
});