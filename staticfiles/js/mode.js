$("input").click(() => {
    if ($("body").hasClass("light")) {
        $("body").removeClass("light");
        $("body").addClass('dark');
        $("#interactive").html("<br>this might be more comfortable. <br><br> turn it back");
        $("#sos").css("color","#CCE5FF");
        $("#sos").css("background","#003366");
        $("#sos").css("border-bottom-color","#002D5B");
        $("sosExplain").css("color","#333333");
        $("#sosExplain").css("background","#CCE5FF");
        $("#exp").css("color","#CCE5FF");
        $("#exp").css("background","#003366");
        $("#exp").css("border-bottom-color","#002D5B");
        $("#expExplain").css("color","#333333");
        $("#expExplain").css("background","#CCE5FF");
        $("#ach").css("color","#CCE5FF");
        $("#ach").css("background","#003366");
        $("#ach").css("border-bottom-color","#002D5B");
        $("#achExplain").css("color","#333333");
        $("#achExplain").css("background","#CCE5FF");
        $("#switch").css("background","#E0E0E0");
        $("#cr").css("color","#CCE5FF");
    }
    else {
        $("body").removeClass("dark");
        $("body").addClass("light");
        $("#interactive").html("<br>Is it too bright? <br><br> turn on dark mode");
        $("#sos").css("color","#333333");
        $("#sos").css("background","#B0C4DE");
        $("#sos").css("border-bottom-color","#9EB0C7");
        $("#sosExplain").css("color","#333333");
        $("#sosExplain").css("background","#FFFFFF");
        $("#exp").css("color","#333333");
        $("#exp").css("background","#B0C4DE");
        $("#exp").css("border-bottom-color","#9EB0C7");
        $("#expExplain").css("color","#333333");
        $("#expExplain").css("background","#FFFFFF");
        $("#ach").css("color","#333333");
        $("#ach").css("background","#B0C4DE");
        $("#ach").css("border-bottom-color","#9EB0C7");
        $("#achExplain").css("color","#333333");
        $("#achExplain").css("background","#FFFFFF");
        $("#switch").css("background","#194775");
        $("#cr").css("color","#333333");
    }
});