from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from .views import index

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
import time

# Create your tests here.
class StorySevenUnitTest(TestCase):
    def test_for_template_and_views(self):
        response = self.client.get('/')
        target = resolve('/')
        self.assertTemplateUsed('myActivities.html')
        self.assertContains(response, 'Get to know my recent activities!', html=True)
        self.assertContains(response, 'SOS', html=True)
        self.assertContains(response, 'What I Have Done', html=True)
        self.assertContains(response, 'What I Achieved', html=True)
        self.assertTrue(target.func, index)

class StorySevenFunctionalTest(LiveServerTestCase):
    def setUp(self):
        # firefox
        super(StorySevenFunctionalTest, self).setUp()
        firefox_options = Options()
        firefox_options.add_argument('--no-sandbox')
        firefox_options.add_argument('--headless')
        firefox_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Firefox(firefox_options = firefox_options)
    
    def tearDown(self):
        time.sleep(5)
        self.selenium.quit()
        super(StorySevenFunctionalTest, self).tearDown()

    def test_click(self):
        selenium = self.selenium
        self.selenium.get('http://127.0.0.1:8000')

        selenium_button1 = selenium.find_element_by_id('sos')
        selenium_button2 = selenium.find_element_by_id('exp')
        selenium_button3 = selenium.find_element_by_id('ach')
        selenium_switch = selenium.find_element_by_id('switch')

        selenium_button1.click()
        time.sleep(5)
        selenium_button2.click()
        time.sleep(5)
        selenium_button3.click()
        time.sleep(5)
        selenium_switch.click()
        time.sleep(5)